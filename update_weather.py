import urllib.request
import os
import json

def download_html(url, path):
    req = urllib.request.Request(url)
    res = urllib.request.urlopen(req)
    data = json.loads(res.read().decode('utf-8'))
    data['latitude'] = data['longitude'] = 0
    with open(path, "w") as f:
        json.dump(data, f)

if __name__ == "__main__":
    apitoken = ""
    home_location = ""
    REPO_DIR = os.getenv('OPENSHIFT_REPO_DIR', '.')
    if REPO_DIR[:-1] != '/':
        REPO_DIR += '/'
        
    path = REPO_DIR+"static/data/weather.json"
    
    with open(REPO_DIR+"apitoken.txt") as f:
        apitoken = f.read()
    with open(REPO_DIR+"homelocation.txt") as f:
        home_location = f.read()
        
    url = "https://api.darksky.net/forecast/" + apitoken + "/" + home_location + "?units=ca&exclude=flags"
    download_html(url, path)
