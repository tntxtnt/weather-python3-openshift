function loadJSON(url, callback) {   
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open('GET', url, true);
  xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {
      return callback(xobj.responseText);
    }
  };
  xobj.send();
}

function sprint02f(d) {
  var dd = "0" + d;
  return dd.substr(-2);
}

function getDay(date) {
  var dow = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  return dow[date.getDay()];
}

function getFull12Hours(timestamp) {
  var date = new Date(timestamp * 1000);
  var h = date.getHours() % 12;
  if (h === 0) h = 12;
  return getDay(date) + ", " + h + ":" + sprint02f(date.getMinutes())
    + (date.getHours() < 12 ? " AM" : " PM");
}

function get12Hours(timestamp) {
  var date = new Date(timestamp * 1000);
  var h = date.getHours() % 12;
  if (h === 0) h = 12;
  return h + (date.getHours() < 12 ? " AM" : " PM");
}

function init() {
  var url = '/static/data/weather.json?r=' + new Date().getTime();
  loadJSON(url, function(response) {
    weather = JSON.parse(response);
    
    var currentDiv = document.createElement("DIV");
    currentDiv.id = "currentlyWeather";
    
    var dateDiv = document.createElement("DIV");
    dateDiv.className = "fontsize24";
    dateDiv.textContent = getFull12Hours(weather.currently.time);
    currentDiv.appendChild(dateDiv);
    
    var tempContainer = document.createElement("DIV");
    tempContainer.id = "currentlyTemperature";
    
    var icon = document.createElement("IMG");
    icon.alt = weather.currently.icon;
    icon.src = "/static/icon/" + weather.currently.icon + ".png";
    icon.title = weather.currently.summary;
    tempContainer.appendChild(icon);
    
    var temp = document.createElement("SPAN");
    temp.textContent = Math.round(weather.currently.temperature) + "°";
    temp.id = "currentTemperature";
    tempContainer.appendChild(temp);
    
    var degree = document.createElement("SUP");
    degree.textContent = "C";
    tempContainer.appendChild(degree);
    
    currentDiv.appendChild(tempContainer);
    
    var precipDiv = document.createElement("DIV");
    precipDiv.className = "fontsize18";
    precipDiv.textContent = "Precipitation: " + Math.round(weather.currently.precipProbability * 100) + "%";
    currentDiv.appendChild(precipDiv);
    
    var humidDiv = document.createElement("DIV");
    humidDiv.className = "fontsize18";
    humidDiv.textContent = "Humidity: " + Math.round(weather.currently.humidity * 100) + "%";
    currentDiv.appendChild(humidDiv);
    
    var windDiv = document.createElement("DIV");
    windDiv.className = "fontsize18";
    windDiv.textContent = "Wind: " + Math.round(weather.currently.windSpeed) + " km/h";
    currentDiv.appendChild(windDiv);
    
    var head = document.getElementById("currentlyWeatherContainer");
    head.appendChild(currentDiv);
    
    var hourlyTimestamp = [];
    var hourlyTemp = ['Temperature'];
    var hourlyPrec = ['Precipitation'];
    var hourlyHumid = ['Humidity'];
    var hourlyWind = ['Wind'];
    weather.hourly.data.forEach(function(e){
      hourlyTimestamp.push(e.time);
      hourlyTemp.push(e.temperature);
      hourlyPrec.push(Math.round(e.precipProbability * 100));
      hourlyHumid.push(Math.round(e.humidity * 100));
      hourlyWind.push(e.windSpeed);
    });
    var chart1 = c3.generate({
      padding: {
        left:35,
        right:45
      },
      size: { height:140 },
      bindto: '#chart1',
      data: {
        columns: [ hourlyTemp ],
        colors: { 
          Temperature: '#fa1'
        },
        type: 'area-spline',
      },
      axis: {
        x: { tick: { format: function(d) { return get12Hours(hourlyTimestamp[d]); } } },
        y: {
          padding: { top:0, bottom:0 },
          tick: { format: function(d) { return Math.round(d) + "°C"; }, count: 6 }
        }
      },
      tooltip: {
        format: {
          title: function(d) { return getFull12Hours(hourlyTimestamp[d]); }
        }
      }
    });//hourly temperature
    var chart2 = c3.generate({
      padding: {
        left:35,
        right:45
      },
      size: { height:125 },
      bindto: '#chart2',
      data: {
        columns: [
          hourlyPrec,
          hourlyHumid,
          hourlyWind,
        ],
        colors: {
          Humidity: '#bcc',
          Precipitation: '#6af',
          Wind: '#8c9'
        },
        axes: {
          Wind: 'y2'
        },
        type: 'spline',
        types: {
          Humidity: 'area-step'
        }
      },
      axis: {
        x:  { tick: { format: function(d) { return get12Hours(hourlyTimestamp[d]); } } },
        y:  {
          min: 0,
          max: 100,
          padding: { top:0, bottom:0 },
          tick: {
            format: function(d) { return d + "%"; },
            count: 5
          }
        },
        y2: {
          tick: {
            format: function(d) { return Math.round(d) + " km/h"; },
            count: 5
          },
          show: true
        }
      },
      tooltip: {
        format: {
          title: function(d) { return getFull12Hours(hourlyTimestamp[d]); },
          value: function (value, ratio, id) {
            var fmt = function(d) { return d; };
            if (id === 'Wind')
              fmt = function(d) { return Math.round(d) + " km/h"; };
            else if (id === 'Precipitation' || id === 'Humidity')
              fmt = function(d) { return d + "%"; };
            else if (id === 'Wind')
              fmt = function(d) { return Math.round(d) + " km/h"; };
            return fmt(value);
          }
        }
      }
    }); //hourly wind, humid, precipProbability
    
    var i = 0;
    weather.daily.data.forEach(function(e){
      console.log(e.windSpeed);
      console.log();
      
      var dayWtContainer = document.getElementById("day"+i);
      dayWtContainer.className += " weather-day text-center";
      
      var dayCtn = document.createElement("DIV");
      dayCtn.className = "text-uppercase";
      var dayText = document.createElement("STRONG");
      dayText.textContent = getDay(new Date(e.time*1000)).substr(0,3);
      dayCtn.appendChild(dayText);
      dayWtContainer.appendChild(dayCtn);
      
      var iconCtn = document.createElement("IMG");
      iconCtn.alt = e.icon;
      iconCtn.src = "/static/icon/" + e.icon + ".png";
      iconCtn.title = e.summary;
      dayWtContainer.appendChild(iconCtn);
      
      var tempMinMaxCtn = document.createElement("DIV");
      tempMinMaxCtn.className = "fontsize16";
      var tmp1 = document.createElement("SPAN");
      tmp1.textContent = Math.round(e.temperatureMax) + "°";
      tempMinMaxCtn.appendChild(tmp1);
      var deg1 = document.createElement("SUP");
      deg1.className = "fontsize10 marginR8";
      deg1.textContent = "C";
      tempMinMaxCtn.appendChild(deg1);
      var tmp2 = document.createElement("SPAN");
      tmp2.textContent = Math.round(e.temperatureMin) + "°";
      tempMinMaxCtn.appendChild(tmp2);
      var deg2 = document.createElement("SUP");
      deg2.className = "fontsize10";
      deg2.textContent = "C";
      tempMinMaxCtn.appendChild(deg2);
      dayWtContainer.appendChild(tempMinMaxCtn);
      
      var otherInfoCtn = document.createElement("DIV");
      
      var precipitationImg = document.createElement("IMG");
      precipitationImg.alt = "p";
      precipitationImg.src = "/static/icon/precipitation.png";
      precipitationImg.width="12";
      precipitationImg.title = "Precipitation";
      otherInfoCtn.appendChild(precipitationImg);
      var precipitationText = document.createElement("SPAN");
      precipitationText.className = "marginL2 marginR6 fontsize10";
      precipitationText.textContent = Math.round(e.precipProbability * 100) + "%";
      otherInfoCtn.appendChild(precipitationText);
      
      var humidityImg = document.createElement("IMG");
      humidityImg.alt = "h";
      humidityImg.src = "/static/icon/humidity.png";
      humidityImg.width="12";
      humidityImg.title = "Humidity";
      otherInfoCtn.appendChild(humidityImg);
      var humidityText = document.createElement("SPAN");
      humidityText.className = "fontsize10";
      humidityText.textContent = Math.round(e.humidity * 100) + "%";
      otherInfoCtn.appendChild(humidityText);
      
      var windInfoCtn = document.createElement("DIV");
      
      var windspeedImg = document.createElement("IMG");
      windspeedImg.alt = "w";
      windspeedImg.src = "/static/icon/windspeed.png";
      windspeedImg.width="12";
      windspeedImg.title = "Wind speed";
      windInfoCtn.appendChild(windspeedImg);
      var windspeedText = document.createElement("SPAN");
      windspeedText.className = "marginL2 fontsize10";
      windspeedText.textContent = Math.round(e.windSpeed) + " km/h";
      windInfoCtn.appendChild(windspeedText);
      
      dayWtContainer.appendChild(otherInfoCtn);
      dayWtContainer.appendChild(windInfoCtn);
      
      i++;
    });
    
  });
}


init();

